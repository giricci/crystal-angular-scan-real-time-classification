import setuptools
from setuptools import setup, find_packages

REQUIREMENTS: dict = {
    'core': [
        "matplotlib",
        "numpy",
        "pandas",
        "keras >= 2.11.0",
        "tensorflow >= 2.11.0",
        "scikit-learn",
        "mlp-model-api"
    ],
    'test': [
        #'pytest',
    ],
    'dev': [
        # 'requirement-for-development-purposes-only',
    ],
    'doc': [
        #'sphinx',
        #'acc-py-sphinx',
    ],
}

setuptools.setup(
    name='crystal-angular-scan-real-time-classification',
    version="0.0.4",
    author='Gianmarco Ricci',
    author_email='gianmarco.ricci@cern.ch',
    description='1D-CNN used for crystal alignment classification',
    long_description=' ',
    long_description_content_type='text/markdown',
    url='',
    package_data={'':
                      ['mlp-models.toml'],
                  },
    packages=find_packages(),
    python_requires='~=3.7',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=REQUIREMENTS['core'],
)
