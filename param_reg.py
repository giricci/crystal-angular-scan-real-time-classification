from mlp_client import AUTO, Client, Profile
import cry_alignment



path = 'C:\\Users\\giricci\\Desktop\\crystal-angular-scan-real-time-classification\\cry_alignment\\2features3classes_model.h5'
model = cry_alignment(path)
model.load_parameters(path)

client = Client(Profile.PRO)
output = client.publish_model_parameters_version(
    model,
    name = 'cry_alignment',
    version = '0.0.4',
)

print(output)
