''' 
© Copyright CERN 2022.  All rights reserved. This software is released under a CERN proprietary
software licence. Any permission to use it shall be granted in writing. Requests shall be
addressed to CERN through mail-KT@cern.ch

Python: 3.10.6
Keras : 2.8.0
Author: Gianmarco Ricci CERN BE/CEM/MRO 2023
===============================================================================================
'''
import mlp_model_api
import numpy as np
import pandas as pd
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
from keras.models import load_model
import tensorflow as tf
from keras.utils import pad_sequences
from sklearn.preprocessing import StandardScaler


__version__ = "0.0.4"

class ChannelingClassification(mlp_model_api.MlpModel):

    def __init__(self):
        """
        Brief: 
        Class cTor
        """
        #self.model_ = load_model(pathToMLModel)

    def predict(self, BLM_data_crystal, BLM_data_secondary):
        """
        Brief: 
        This method is used to classify a BLM signal.
        The Machine learning model is able to classify the BLM signal given in input in 3 classes:
        Class 0: BLM Steady State;
        Class 1: Partial Well;
        Class 2: Channeling Well.

        Input: 
        BLM_data_crystal (list)  : BLM signal registered on the crystal.
        BLM_data_secondary (list): BLM signal registered on the crystal.

        Returns:
        Return (float)           : prob_c0 - Probability that the BLM signal belongs to Class 0.
        Return (float)           : prob_c1 - Probability that the BLM signal belongs to Class 1.
        Return (float)           : prob_c2 - Probability that the BLM signal belongs to class 0.
        Return (int)             : predicted_class - Predicted class (0-2).
        """
        dictionary = {
        'BLM Secondary [Gy/s]': BLM_data_secondary,
        'BLM Crystal [Gy/s]'  : BLM_data_crystal,
        }
        df_blm = pd.DataFrame(dictionary)
        df_blm_scaled = pd.DataFrame(StandardScaler().fit_transform(df_blm.iloc[:, :]))
        df_blm_scaled = np.array(df_blm_scaled)
        df_blm_scaled = df_blm_scaled.reshape((1, len(df_blm_scaled), 2))
        df_blm_scaled = pad_sequences(df_blm_scaled, maxlen = 630, padding = "pre", dtype = "float")
        # --- Predict --- #
        prediction = self.model_.predict(df_blm_scaled)
        prob_c0 = prediction[0][0]
        prob_c1 = prediction[0][1]
        prob_c2 = prediction[0][2]
        predicted_class = prediction.argmax()
        return prob_c0, prob_c1, prob_c2, predicted_class
    
    def load_parameters(self, parameters_src)  -> None:
        """
        Brief: 
        Method used to load the the ML model from a .h5 file.

        Input:
        parameters_src (string): path to the location where the .h5 file is stored.
        """
        self.model_ = load_model(parameters_src)

    def export_parameters(self, parameters_target)  -> None:
        """
        Brief: 
        Method used to save the the ML model stored in a .h5 file.

        Input:
        parameters_target (string): path to the location where the .h5 file will be stored.
        """
        self.model_.save(parameters_target)




    

